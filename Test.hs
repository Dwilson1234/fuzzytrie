{-# LANGUAGE FlexibleInstances #-}
import FuzzyTrie
import qualified Data.Text as Text
import Data.Text (Text)
import Data.Maybe
import qualified Data.List as L

instance TrieLabel [Text] where
    testLabel _ [] = Nothing
    testLabel [] _ = Nothing
    testLabel a b = Just (take (go a b) a, drop (go a b) a, drop (go a b) b)
        where
            go _ [] = 0
            go [] _ = 0
            go (a:as) (b:bs)
                | a == b = 1 + go as bs
                | otherwise = 0

    isContained = L.isInfixOf

    isEmpty = L.null
