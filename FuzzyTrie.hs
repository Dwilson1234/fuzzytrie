{-# LANGUAGE ViewPatterns #-}
module FuzzyTrie where

import qualified Data.Text as T
import Data.Text (Text)
import Data.List (nub)

data Trie b a = 
    Branch (b, a) 
        (Trie b a) -- child
        (Trie b a) -- sibling
        |
    Empty
    deriving Show

class TrieLabel b where
    testLabel :: b -> b -> Maybe (b, b, b)  -- tests a label to determine what to do Data.Text.commonPrefixes
    isContained :: b -> b -> Bool   -- tests if a query is in a label. Data.Text.isInfixOf
    isEmpty :: b -> Bool            -- empty label

instance TrieLabel Text where
    testLabel = T.commonPrefixes
    isContained = T.isInfixOf
    isEmpty x = x == T.empty

class TrieNode a where
    mergeNode :: a -> a -> a    -- merge two nodes

key :: Trie b a -> b
key (Branch (k, _) _ _ ) = k

value :: Trie b a -> a
value (Branch (_, v) _ _ ) = v

child :: Trie b a -> Trie b a
child (Branch _ x _) = x

next :: Trie b a -> Trie b a
next (Branch _ _ x) = x

singleton :: (TrieNode a) => b -> a -> Trie b a
singleton t x = Branch (t, x) Empty Empty

insert :: (TrieNode a, TrieLabel b) => b -> a -> Trie b a -> Trie b a
insert k v Empty = Branch (k, v) Empty Empty
insert k v (Branch (kt, vt) ct nt) =
    case k `testLabel` kt of
        -- (prefix, k suffix, kt suffix)
        Just (p, (isEmpty -> False), (isEmpty -> False)) 
            -> Branch (p, mergeNode v vt) ct nt   --both entries are the same, just update node
        Just (p, ks, (isEmpty -> False)) 
            -> Branch (p, mergeNode v vt) (insert ks v ct) nt -- new entry is longer, add it to children
        Just (p, (isEmpty -> False), kts) 
            -> Branch (p, mergeNode v vt) (Branch (kts, vt) ct nt) Empty -- old entry is longer, make a new node with the old node as first child
        Just (p, ks, kts) 
            -> Branch (p, mergeNode v vt) -- neither entry is a complete prefix, make a new node for prefix, then add both suffixes as children
                    (Branch (ks, v) Empty 
                    (Branch (kts, vt) ct Empty)
                    ) nt
        Nothing -> Branch (kt, vt) ct (insert k v nt)

find :: (TrieLabel b) => b -> Trie b a -> Maybe a
find k Empty = Nothing
find k (Branch (kt, vt) ct Empty) =
    case k `testLabel` kt of
        Just (_, (isEmpty -> False), (isEmpty -> False))
            -> Just vt -- found it
        Just (_, ks, (isEmpty -> False))
            -> find ks ct -- search children
        otherwise -> Nothing
find k (Branch (kt, vt) ct nt) =
    case k `testLabel` kt of
        Just (_, (isEmpty -> False), (isEmpty -> False))
            -> Just vt -- found it
        Just (_, ks, (isEmpty -> False))
            -> case (find ks ct) of
                   Just x -> Just x
                   Nothing -> find k nt
        otherwise -> find k nt -- other cases all require searching siblings

-- Find all instances where this could be
findFuzzy :: (Eq a, TrieLabel b) => b -> Trie b a -> [a]
findFuzzy k Empty = []
findFuzzy k (Branch (kt, vt) ct nt) =
    nub $
        findFuzzy k nt
        ++
        if k `isContained` kt
            then [vt] -- found it
            else findFuzzy k ct -- search children
